#!/usr/bin/env fry

# @file remusi.fat
# @brief Offers a streamlined approach to "REMove Unused Scala Imports"
# @author Antonio Prates <hello@aprates.dev>
# @version 1.0.1
# @date 2024-09-27
# @copyright MIT - https://gitlab.com/aprates/remusi

_       <- fat.type._
file    <- fat.file
system  <- fat.system
console <- fat.console

isDebug = false

# Define a Location type to hold file path, line number, and column number
# Note: methods expect ln and col to use zero-based indexing
Location = (path: Text, ln: Number, col: Number)

# Method to extract location (file path, line, column) from a warning line
extractLocation = (line: Text): Location -> {
  # Check if the line starts with a warning tag
  line.startsWith('[warn] ') => {
    # Split fragment after warn-tag into parts using ':' as a delimiter
    strs = line(7..).split(':')

    # If there are exactly 4 parts, construct a Location object
    # Subtract 1 from line and column numbers to convert to zero-based indexing
    strs.size == 4 => Location(strs(0), Number(strs(1))-1, Number(strs(2))-1)
    _              => Error('bad input: line has unexpected format')
  }
  _ => Error('bad input: line does not start with warn tag')
}

# Method to remove the unused import name from a given line and position
removeFromLineAt = (line: Text, col: Number): Text -> {
  # Check if the line contains a comma, indicating multiple imports
  line.contains(',') => {
    # Further check if the line contains the 'import' keyword
    line.contains('import') => {
      # Split the line at the column where the unused import is found
      partA = line(..<col)
      partB = line(col..)

      # Find the end of import name by looking for a comma or closing brace
      ~ end = partB.indexOf(', ')

      # If no comma is found, look for a closing brace
      end == -1 => {
        end = partB.indexOf('}')

        # If no closing brace is found either, raise an error
        end == -1 => Error('bad input: {line}')
        # Otherwise, remove the unused import and handle the closing brace
        _ => (partA + partB(end..)).replace(', }', '}')
      }
      # If a comma is found, remove the unused import up to the next comma
      _ => partA + partB((end + 2)..)
    }
  }
}

# Method to fix unnecessary braces around imports
fixBraces = (line: Text): Text -> {
  line => {
    !line.contains(',') &
    !line.contains('=>') &
    line.contains('\{') &
    line.contains('}') => {
      line - '}' - '\{'
    }
    _ => line
  }
}

# Method to remove an unused import keyword at a given location in a file
removeKeywordAt = (location: Location): Void -> {
  sourceLines = file.read(location.path).split('\n')
  originalLine = sourceLines(location.ln)
  updatedLine = fixBraces(removeFromLineAt(originalLine, location.col))

  isDebug ? {
    console.log('\ncode at : {location.path}')
    console.log('original: {originalLine}')
    console.log('updated : {updatedLine}')
  }

  # Replace the original line with the updated line in the list of source lines
  updatedLines = (
    sourceLines(..<location.ln)
    + [ updatedLine ]
    + sourceLines((location.ln + 1)..)
  )

  file.write(location.path, updatedLines.join('\n'))
}

# Main Method that processes a given file path to remove unused imports
system.args @ path -> {
  console.log('processing {path}...')

  # Read, split into lines, filter for 'Unused import', and reverse
  filteredLines = file.read(path)
    .split('\n')
    .filter(-> _.contains(': Unused import'))
    .reverse

  # For each filtered line, extract the location and remove the unused import
  filteredLines @ extractLocation @ removeKeywordAt

  null
}
