# remusi

## Overview

`remusi` offers a streamlined approach to "REMove Unused Scala Imports" by analyzing `sbt` logs, automating the tedious task of manually looking through log warnings to identify and eliminate unnecessary import statements, which enhances codebase maintainability, compile speed and unclutters the compiler logs to drive attention to more relevant issues.

Contrary to the `scalafix` sbt plugin's `RemoveUnused` command, which depends on `SemanticDB` and may considerably extend operation times, `remusi` directly utilizes sbt logs for a more performant approach. While this method might not provide the same level of safety, it offers a plain-simple quick-dirty alternative for cleaning imports. To safeguard your code, it's recommended to employ a version control system (e.g. git) and commit your files before running `remusi`, then recompile to validate code changes.

## Features

- **Log Analysis**: Parses sbt logs provided via file to detect unused imports.
- **Automated Removal**: Automatically removes unused imports from source files.
- **Brace Handling**: Intelligently corrects unnecessary braces around imports.

## Requirements

To use `remusi`, ensure you have `fry` (>2.1.1), the FatScript interpreter, and `sbt` installed on your system. For `fry` installation, visit the [official FatScript website](https://fatscript.org) for detailed instructions.

## Installation

1. Clone or download the `remusi.fat` script to your local machine.
2. Make sure the script has execute permissions:

```bash
chmod +x remusi.fat
```

## Usage

To use the `remusi` utility, simply run the script with the path to a sbt log file as an argument:

```bash
./remusi.fat /path/to/your/sbt.log
```

The script will process the log file, identify any unused imports in your Scala files, and remove them automatically.

### Creating the log file

A simple way to achieve this is by piping the result of compile command to a file:

```bash
sbt "project <my_project>" "clean" "compile" > sbt.log
```

Also, you can just literally copy-paste sbt compile logs into a file 😎

## Debugging

To see logs of the changes being made, set the `isDebug` option to `true` within the script.

## Donations

Did you find `remusi` useful and would like to say thanks?

[Buy me a coffee](https://www.buymeacoffee.com/aprates)

## License

[MIT](LICENSE) © 2024 Antonio Prates.
